# The faster and easier web framework for golang



     __  __                  _
    |  \/  |_   _ _ __ _ __ | |__  _   _
    | |\/| | | | | '__| '_ \| '_ \| | | |
    | |  | | |_| | |  | |_) | | | | |_| |
    |_|  |_|\__,_|_|  | .__/|_| |_|\__, |
                      |_|          |___/    V1.0.6,💕By Oshine
                                            leave me message-> https://space.bilibili.com/28592193

>🎈🎈🎈🎈
> 
>This project is based on Redis SSDB and relational databases such as MySQL PostgreSQL, which can be used to develop large projects with extremely fast development speed
>
>When running this project, Redis and ssdb cache databases must be installed. As for relational databases, MySQL can be used
>
>Installing MySQL Redis and SSDB can be done using Docker
> 


## ❤️💯.) PrintLn("Attention Please")：
#### 1.About Compile：

🪶 When using a database, the Murphy framework compilation requires the addition of a compilation condition - tags

🪶 The currently supported tags are MySQL ✔️ , postgres (waiting)

🪶 If you use MySQL database in the project, the correct compilation command is as follows:

>Wrong： `go build main.go` ❌
>
>Correct： `go build --tags mysql main.go` ✔️

#### 2.Install：
### *.Docker install mysql
```shell
docker run -p 8060:3306 --name mariadb \
--log-opt max-size=1g \
-e MYSQL_ROOT_PASSWORD=123456 \
-d docker.io/mariadb:10.5.12-focal
```

### *.Docker install redis and ssdb
```shell
#ssdb(amd64):
docker run -dit -p 8888:8888 --name ssdb leobuskin/ssdb-docker

#ssdb(arm64):
docker run -dit -p 8888:8888 --name ssdb r3v3r/ssdb:test-1.9.8
#ssdb(arm64) add dir map:
docker run -dit -p 8888:8888 --name ssdb1 -v /data/ssdb/var:/ssdb/var r3v3r/ssdb:test-1.9.8

#redis(amd64 and arm64):
docker run -dit -p 6379:6379 --name redis redis:5.0.14-alpine

```
 
### 1.) Http Server + Websocket Server example ❤️ ：https://gitee.com/oshine/murphy-law 

### 2.) Murphy web framework features:
1. Support http router && group && middleware     ✅
2. Support websocket     ✅
3. Support mysql         ✅
4. Support redis         ✅
5. Support ssdb          ✅
6. Support model         ✅
7. Support model auto generate     ✅
8. Support pagination   ✅
9. Support auto cache   ✅
10. Support async task   ✅
11. Support mssql         🪶
12. Support postgres      🪶
13. Support sqlite        🪶

### Http Server Demo🍔：
> 运行以下代码，访问浏览器：http://127.0.0.1:8000/api/home/index
```go
package main

import (
	"gitee.com/oshine/murphy/core"
	"gitee.com/oshine/murphy/core/text"
	"github.com/valyala/fasthttp"
	"log"
)

type HomeController struct {
}

func (mc *HomeController) Init() {

}

func (mc *HomeController) Index(ctx *fasthttp.RequestCtx) error {

	method := string(ctx.Request.Header.Method())

	log.Println(method)

	if text.InArray(method, []string{"GET", "POST", "PUT", "DELETE"}) {

		ctx.Response.SetBodyString("Hello World")

		return nil
	}

	ctx.Response.SetBodyString("Not Support Method:" + method)

	return nil
}

func main() {
	svr := core.Server{}
	svr.Http().
		Group("/api", func(group *core.SvrGroup) {

			group.Home("/home/index") // let "/" = api/"home/index"

			group.Bind(
				&HomeController{}, // home
				//&OtherController{},
				//&***Controller{},
			)

		}).RunTLS("127.0.0.1:8000", "", "")

}


```

### Websocket Server Demo🍔：
```go
package main

import (
	"gitee.com/oshine/murphy/core"
	"github.com/fasthttp-contrib/websocket"
)

func WsV1(c *websocket.Conn) error {
	err := c.WriteMessage(websocket.TextMessage, []byte("Hello World!"))
	if err != nil {
		return err
	}
	return nil
}

func main() {
	svr := core.Server{}
	svr.Ws().
		//MiddleWare(WsV1).
		Router("/ws-v1", WsV1).
		//Router("/ws-v2", ws.WsV2).
		RunTLS("127.0.0.1:8006", "", "")
}


```

## 5.) mysql database && redis、ssdb
**1.create file db-main-config.yaml at root dir.**
```yaml
database:
  adapter: mysql
  user: root
  pass: 1qaz.
  host: 127.0.0.1
  port: 3306
  db: "oilcn"
  prefix: ""

redis:
  host: 127.0.0.1
  port: 6379
  db: 0
  init: 10
  maxCon: 60
  idle: 10

ssdb:
  host: 127.0.0.1
  port: 6379
  db: 0
  init: 10
  maxCon: 60
  idle: 10

```
**2.use db manager from murphy framework.**
```go
package main

import "gitee.com/oshine/go-murphy/core/mdb"

func main(){
    if !mdb.ParseDbConfigAndInit("db-main-config.yaml") {
        fmt.Println("初始化数据库失败/init database failed")
        return
    }
    // 数据库连接池初始化成功

    mdb.DbHandler.Db.Query("select * from user") // mysql
	
    mdb.DbHandler.GetRds1().Do("set","key","val") // redis
    mdb.DbHandler.GetSsdb1().Do("set","key","val") // ssdb
	
	
	
	
}

```