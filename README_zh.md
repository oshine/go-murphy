# Golang Web框架 墨菲( Murphy )

**适用于要求高性能&&快速开发的一款web框架**


     __  __                  _
    |  \/  |_   _ _ __ _ __ | |__  _   _
    | |\/| | | | | '__| '_ \| '_ \| | | |
    | |  | | |_| | |  | |_) | | | | |_| |
    |_|  |_|\__,_|_|  | .__/|_| |_|\__, |
                      |_|          |___/    V1.0.6,💕By Oshine
                                            leave me message-> https://space.bilibili.com/28592193

> 本项目底层基于redis ssdb 和关系型数据库，如mysql postgres 等，可用于开发大型项目，且开发速度极快
> 
> 本项目运行时，`必须安装redis和ssdb缓存数据库`，至于关系型数据库可以使用 `mysql`
> 
> 安装mysql redis和ssdb可以使用docker安装：


## ❤️💯.) PrintLn("说在前面")：
#### 1.关于编译：

🪶在使用到数据库时，murphy框架编译需要加上编译条件 --tags

🪶目前支持的tags有 mysql✔️ postgres (等待)

🪶如果项目中使用到了mysql数据库，那么正确的编译命令如下(默认数据库为mysql，如果缓存postgres则做相应的更换即可)：

>错误编译： `go build main.go` ❌   默认mysql驱动，即使可以编译成功，也不推荐这样演绎，必须使用--tags来控制
> 
>正确编译： `go build --tags mysql main.go` ✔️    使用mysql驱动编译\
>正确编译： `go build --tags postgres main.go` ✔️  使用postgres驱动编译

#### 2.环境安装：
### *.Docker 安装mysql （这里安装的是mysql的一个分支：mariadb，也可以选择mysql或其他分支）
```shell
docker run -p 8060:3306 --name mariadb \
--log-opt max-size=1g \
-e MYSQL_ROOT_PASSWORD=123456 \
-d docker.io/mariadb:10.5.12-focal
```

### *.Docker 安装ssdb和redis
```shell
#ssdb(amd64):
docker run -dit -p 8888:8888 --name ssdb leobuskin/ssdb-docker

#ssdb(arm64):
docker run -dit -p 8888:8888 --name ssdb r3v3r/ssdb:test-1.9.8
#ssdb(arm64) 添加映射目录:
docker run -dit -p 8888:8888 --name ssdb1 -v /data/ssdb/var:/ssdb/var r3v3r/ssdb:test-1.9.8

#redis(amd64 and arm64):
docker run -dit -p 6379:6379 --name redis redis:5.0.14-alpine

```


## 1.) Http服务端+Websocket服务端 复合案列 ❤️ ：https://gitee.com/oshine/free-api

## 2.) Murphy web 框架能力:
1. Support http router && group && middleware     ✅
2. Support websocket     ✅
3. Support mysql         ✅
4. Support redis         ✅
5. Support ssdb          ✅
6. Support model         ✅
7. Support model auto generate     ✅
8. Support pagination   ✅
9. Support auto cache   ✅
10. Support async task   ✅
11. Support mssql         🪶
12. Support postgres      🪶
13. Support sqlite        🪶



### 3.) Http Server 快速开始🍔：
> 运行以下代码，访问浏览器：http://127.0.0.1:8000/api/home/index
```go
package main

import (
	"gitee.com/oshine/murphy/core"
	"gitee.com/oshine/murphy/core/text"
	"github.com/valyala/fasthttp"
	"log"
)

type HomeController struct {
}

func (mc *HomeController) Init() {

}

func (mc *HomeController) Index(ctx *fasthttp.RequestCtx) error {

	method := string(ctx.Request.Header.Method())

	log.Println(method)

	if text.InArray(method, []string{"GET", "POST", "PUT", "DELETE"}) {

		ctx.Response.SetBodyString("Hello World")

		return nil
	}

	ctx.Response.SetBodyString("Not Support Method:" + method)

	return nil
}

func main() {
	svr := core.Server{}
	svr.Http().
		Group("/api", func(group *core.SvrGroup) {

			group.Home("/home/index") // let "/" = api/"home/index"

			group.Bind(
				&HomeController{}, // home
				//&OtherController{},
				//&***Controller{},
			)

		}).RunTLS("127.0.0.1:8000", "", "")

}


```

### 4.) Websocket Server 快速开始🍔：
```go
package main

import (
	"gitee.com/oshine/murphy/core"
	"github.com/fasthttp-contrib/websocket"
)

func WsV1(c *websocket.Conn) error {
	err := c.WriteMessage(websocket.TextMessage, []byte("Hello World!"))
	if err != nil {
		return err
	}
	return nil
}

func main() {
	svr := core.Server{}
	svr.Ws().
		//MiddleWare(WsV1).
		Router("/ws-v1", WsV1).
		//Router("/ws-v2", ws.WsV2).
		RunTLS("127.0.0.1:8006", "", "")
}


```

## 5.) mysql数据库 && redis、ssdb缓存
1.根目录中新建配置文件 db-main-config.yaml
```yaml
database:
  adapter: mysql
  user: root
  pass: 1qaz.
  host: 127.0.0.1
  port: 3306
  db: "oilcn"
  prefix: ""

redis:
  host: 127.0.0.1
  port: 6379
  db: 0
  init: 10
  maxCon: 60
  idle: 10

ssdb:
  host: 127.0.0.1
  port: 6379
  db: 0
  init: 10
  maxCon: 60
  idle: 10

```
2.使用框架提供的方法自动管理数据库连接
```go
package main

import "gitee.com/oshine/murphy/core/mdb"

func main(){
    if !mdb.ParseDbConfigAndInit("db-main-config.yaml") {
        fmt.Println("初始化数据库失败/init database failed")
        return
    }
    // 数据库连接池初始化成功

    mdb.DbHandler.Db.Query("select * from user") // mysql
	
    mdb.DbHandler.GetRds1().Do("set","key","val") // redis
    mdb.DbHandler.GetSsdb1().Do("set","key","val") // ssdb
	
	
	
	
}

```

## 6.其他功能
**1.redis锁**
```go
//登录限制用户2秒内只能登录一次
if !mdb.TryLock("login_limit", username, 2000) {
    return self.RenderJson(nil).Err("操作过快").Out()
}
```