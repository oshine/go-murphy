package def

import (
	"fmt"
	"strings"
)

type (
	Map        = map[string]interface{}      // Map is alias of frequently-used map type map[string]interface{}.
	MapAnyAny  = map[interface{}]interface{} // MapAnyAny is alias of frequently-used map type map[interface{}]interface{}.
	MapAnyStr  = map[interface{}]string      // MapAnyStr is alias of frequently-used map type map[interface{}]string.
	MapAnyInt  = map[interface{}]int         // MapAnyInt is alias of frequently-used map type map[interface{}]int.
	MapStrAny  = map[string]interface{}      // MapStrAny is alias of frequently-used map type map[string]interface{}.
	MapStrStr  = map[string]string           // MapStrStr is alias of frequently-used map type map[string]string.
	MapStrInt  = map[string]int              // MapStrInt is alias of frequently-used map type map[string]int.
	MapIntAny  = map[int]interface{}         // MapIntAny is alias of frequently-used map type map[int]interface{}.
	MapIntStr  = map[int]string              // MapIntStr is alias of frequently-used map type map[int]string.
	MapIntInt  = map[int]int                 // MapIntInt is alias of frequently-used map type map[int]int.
	MapAnyBool = map[interface{}]bool        // MapAnyBool is alias of frequently-used map type map[interface{}]bool.
	MapStrBool = map[string]bool             // MapStrBool is alias of frequently-used map type map[string]bool.
	MapIntBool = map[int]bool                // MapIntBool is alias of frequently-used map type map[int]bool.
)

type Cond2 [][2]any
type Cond3 [][3]any

type CondWhat struct {
	sub  []ICond
	Cond ICond
	lc   byte // 1 and ,2 or
}

func (c *CondWhat) Append(ic ICond) *CondWhat {
	c.sub = append(c.sub, ic)
	return c
}

func (c *CondWhat) What(ic ICond) *CondWhat {
	cw := &CondWhat{Cond: ic}

	c.sub = append(c.sub, cw)
	return cw
}

func (c *CondWhat) And() *CondWhat {
	c.lc = 0x01
	return c
}

func (c *CondWhat) Or() *CondWhat {
	c.lc = 0x02
	return c
}

// Build    (name="jacky" or user_id=186)  or
func (c *CondWhat) Build() string {
	str := " "
	if c.Cond != nil {
		str = "("
		p1 := c.Cond.Build()
		str += p1[:len(p1)-3]
		str += ")"

		if c.lc == 0x01 {
			str += " and"
		} else {
			str += " or "
		}
	}

	if len(c.sub) > 0 {
		for _, sub := range c.sub {
			p1 := sub.Build()
			str += p1[:len(p1)-3]

			if c.lc == 0x01 {
				str += " and"
			} else {
				str += " or "
			}
		}

	}

	return str
}

func (c *Cond2) Build() string {
	str := ""
	for _, sub := range *c {
		var p string
		switch sub[1].(type) {
		case string:
			p = "\"" + strings.ReplaceAll(sub[1].(string), "\"", "\\\"") + "\""
		default:
			p = fmt.Sprintf("%v", sub[1])
		}
		str += " " + sub[0].(string) + "=" + p + " and"
	}

	return str
}

func (c *Cond3) Build() string {
	str := ""
	for _, sub := range *c {
		p := ""
		switch sub[2].(type) {
		case string:
			p = "\"" + strings.ReplaceAll(sub[2].(string), "\"", "\\\"") + "\""
		default:
			p = fmt.Sprintf("%v", sub[2])
		}
		str += " " + sub[0].(string) + " " + sub[1].(string) + " " + p + " and"
	}

	return str
}

type ICond interface {
	Build() string
}

type (
	List        = []Map        // List is alias of frequently-used slice type []Map.
	ListAnyAny  = []MapAnyAny  // ListAnyAny is alias of frequently-used slice type []MapAnyAny.
	ListAnyStr  = []MapAnyStr  // ListAnyStr is alias of frequently-used slice type []MapAnyStr.
	ListAnyInt  = []MapAnyInt  // ListAnyInt is alias of frequently-used slice type []MapAnyInt.
	ListStrAny  = []MapStrAny  // ListStrAny is alias of frequently-used slice type []MapStrAny.
	ListStrStr  = []MapStrStr  // ListStrStr is alias of frequently-used slice type []MapStrStr.
	ListStrInt  = []MapStrInt  // ListStrInt is alias of frequently-used slice type []MapStrInt.
	ListIntAny  = []MapIntAny  // ListIntAny is alias of frequently-used slice type []MapIntAny.
	ListIntStr  = []MapIntStr  // ListIntStr is alias of frequently-used slice type []MapIntStr.
	ListIntInt  = []MapIntInt  // ListIntInt is alias of frequently-used slice type []MapIntInt.
	ListAnyBool = []MapAnyBool // ListAnyBool is alias of frequently-used slice type []MapAnyBool.
	ListStrBool = []MapStrBool // ListStrBool is alias of frequently-used slice type []MapStrBool.
	ListIntBool = []MapIntBool // ListIntBool is alias of frequently-used slice type []MapIntBool.
)

type (
	Slice    = []interface{} // Slice is alias of frequently-used slice type []interface{}.
	SliceAny = []interface{} // SliceAny is alias of frequently-used slice type []interface{}.
	SliceStr = []string      // SliceStr is alias of frequently-used slice type []string.
	SliceInt = []int         // SliceInt is alias of frequently-used slice type []int.
)

type (
	Array    = []interface{} // Array is alias of frequently-used slice type []interface{}.
	ArrayAny = []interface{} // ArrayAny is alias of frequently-used slice type []interface{}.
	ArrayStr = []string      // ArrayStr is alias of frequently-used slice type []string.
	ArrayInt = []int         // ArrayInt is alias of frequently-used slice type []int.
)
