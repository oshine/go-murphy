package def

import (
	"strings"
	"testing"
)

func TestCond(t *testing.T) {
	what := CondWhat{Cond: &Cond2{{"p1", 1}, {"p2", 1}}}

	what.And().
		What(&Cond2{{"p", 1}, {"p", 1}}).Append(&Cond2{{"p", 1}, {"p", 1}}).
		Or().
		What(&Cond3{{"p", "=", "1"}}).
		And().
		What(&Cond3{{"p", "like", "\"1"}})

	str := what.Build()

	f := str[:len(str)-3]
	v := strings.ReplaceAll(f, " ", "")
	println(f)
	println(v)

	if v != "(p1=1andp2=1)and(p=1andp=1)orp=1andp=1or(p=1)and(plike\\\"1)" {
		t.Fatal("not empty")
	}

	what = CondWhat{}

	what.And().Append(&Cond3{{"p", "=", 1}, {"p", "=", 1}}).Append(&Cond2{{"p", 1}, {"p", 1}}).Append(&Cond2{{"p", 1}, {"p", 1}})

	str = what.Build()

	f = str[:len(str)-3]
	v = strings.ReplaceAll(f, " ", "")
	println("")
	println(f)
	println(v)

	if v != "p=1andp=1andp=1andp=1andp=1andp=1" {
		t.Fatal("not empty")
	}

}
