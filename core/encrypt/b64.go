package encrypt

import (
	"encoding/base64"
	"github.com/rhettli/php2go/php"
)

func B64Str(b []byte) *string {
	var p = php.Base64Encode(b)
	return &p
}

func B64Bytes(b []byte) []byte {
	dst := make([]byte, base64.StdEncoding.EncodedLen(len(b)))
	base64.StdEncoding.Encode(dst, b)
	return dst
}
