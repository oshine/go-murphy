package encrypt

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"fmt"
)

// 填充字符串（末尾）
func PaddingText1(str []byte, blockSize int) []byte {
	//需要填充的数据长度
	paddingCount := blockSize - len(str)%blockSize
	//填充数据为：paddingCount ,填充的值为：paddingCount
	paddingStr := bytes.Repeat([]byte{byte(paddingCount)}, paddingCount)
	newPaddingStr := append(str, paddingStr...)
	//fmt.Println(newPaddingStr)
	return newPaddingStr
}

// 去掉字符（末尾）
func UnPaddingText1(str []byte) []byte {
	n := len(str)
	count := int(str[n-1])
	newPaddingText := str[:n-count]
	return newPaddingText
}

func aesDecode(src, key, iv []byte) ([]byte, error) {
	//client := checkClient(L)

	//iv := []byte(L.ToString(4))                  //"1234567890abcdef";
	//key := []byte(L.CheckString(3))              //"abcdef1234567890";

	//src, _ := php.Base64Decode(L.CheckString(2)) // 123123

	iv, key = padding(iv, key)

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blockMode := cipher.NewCBCDecrypter(block, iv)
	blockMode.CryptBlocks(src, src)
	src = UnPaddingText1(src)

	return src, nil
}

func aesEncode(str, key, iv []byte) ([]byte, error) {
	//client := checkClient(L)

	//str := []byte(L.CheckString(2)) // 123123
	//key := []byte(L.CheckString(3)) //"abcdef1234567890";
	//iv := []byte(L.ToString(4))     //"1234567890abcdef";

	iv, key = padding(iv, key)

	fmt.Println([16]byte{})

	fmt.Println(iv, key, str)
	fmt.Println(len(iv), len(key))

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	str = PaddingText1(str, block.BlockSize())
	var blockMode = cipher.NewCBCEncrypter(block, iv)

	blockMode.CryptBlocks(str, str)

	return B64Bytes(str), nil
}

func padding(iv, key []byte) ([]byte, []byte) {
	l := len(key)
	originLen := l
	if l > 24 {
		l = 32
	} else if l > 16 {
		l = 24
	} else {
		l = 16
	}

	//初始化一个空的字节数组,这里的16为AES-128-CBC的要求的key的长度
	var keyByte []byte

	ivLen := len(iv)
	for i := 0; i < l; i++ {
		if i < originLen {
			keyByte = append(keyByte, key[i])
		} else {
			keyByte = append(keyByte, 0)
		}
		if i > ivLen-1 {
			iv = append(iv, 0)
		}
	}
	return iv, keyByte
}
