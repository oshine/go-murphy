package mod

type IBaseModel interface {
	ToDetailJson() any
	ToSimpleJson() any
}

type JsonPage struct {
	Entry    interface{} `json:"entry"`
	Total    int64       `json:"total"`
	PageSize int64       `json:"page_size"`
	Page     int64       `json:"page"`
}
