package mdb

import (
	"errors"
	"github.com/gomodule/redigo/redis"
	"io"
	"log"
	"time"
)

func TryLock(lock_key string, request_id string, timeout int) bool {

	//var SET_IF_NOT_EXIST = "NX"
	//var SET_WITH_EXPIRE_TIME = "PX"

	var rds = DbHandler.getRds0()
	defer rds.Close()

	//--加锁
	r, _ := rds.Do("set", lock_key, request_id, "NX", "PX", timeout)

	if r == nil {
		return false
	}

	if "OK" == r.(string) {
		return true
	}

	return false
}

const (
	SET_IF_NOT_EXIST     = "NX"
	SET_WITH_EXPIRE_TIME = "PX"
)

func Lock(lock_key string, request_id string, fun func()) {

	var expireTime = 10000

	var rds = DbHandler.getRds0()
	defer rds.Close()

	for {
		//--加锁
		r, _ := rds.Do("set", lock_key, request_id, SET_IF_NOT_EXIST, SET_WITH_EXPIRE_TIME, expireTime)

		if rv, ok := r.(string); ok && rv == "OK" {
			fun()
		} else {

			time.Sleep(time.Millisecond * 100)
			continue
		}

		//var status, rf = pcall(func, ...)

		var script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end"

		var getScript = redis.NewScript(1, script)

		//-- 解锁
		r1, err := getScript.Do(rds, lock_key, request_id)
		if err != nil {
			log.Println(err.Error())
		}

		if 1 != r1.(int64) {
			log.Println("try unlock error:", r1)
		}
		break
	}
}

func CopyLimit(dst io.Writer, src io.Reader, length int64) (int64, error) {
	var b = make([]byte, 1024)
	var t int64
	for {
		n, err := src.Read(b)
		if err != nil {
			if err != io.EOF {
				return t, err
			} else {
				return t, nil
			}
		}
		t += int64(n)
		if t > length {
			return t, errors.New("enough size")
		}
		_, err = dst.Write(b[:n])
		if err != nil {
			return t, err
		}
	}
}
