package mdb

import (
	"fmt"
	"github.com/goccy/go-json"
	"github.com/gomodule/redigo/redis"
	"github.com/google/uuid"
	"strconv"
	"strings"
	"time"
)

type IAsync interface {
	AsyncStart(p *DelayObj)
}

type AsyncParams map[string]any

type DelayObj struct {
	Name   string
	tm     int
	Action string
	Params *AsyncParams
}

func (p *AsyncParams) GetString(key string) string {
	if r, ok := (*p)[key].(string); ok {
		return r
	}
	return ""
}

func (p *AsyncParams) GetInt(key string) int {
	switch r := (*p)[key]; r.(type) {
	case int64:
		return int(r.(int64))
	case float64:
		return int(r.(float64))
	case int:
		return r.(int)
	}

	return 0
}

func (p *AsyncParams) GetInt64(key string) int64 {
	r := (*p)[key]
	switch r.(type) {
	case int64:
		return r.(int64)
	case float64:
		return int64(r.(float64))
	case int:
		return int64(r.(int))
	}

	return 0
}

func (p *AsyncParams) GetFloat64(key string) float64 {
	if r, ok := (*p)[key].(float64); ok {
		return r
	}
	return 0
}
func (p *AsyncParams) GetAny(key string) any {
	return (*p)[key]
}

func (m *DelayObj) SetTm(t ...int) {
	var f int
	if len(t) > 0 {
		f = t[0]
	} else {
		f = 0
	}
	m.tm = f
}

func (m *DelayObj) WithParam(p *AsyncParams) *DelayObj {
	m.Params = p
	return m
}

const DEFAULT_QUEUE_NAME = "tube_queue"
const START_QUEUE_NAME = "queue_task_ids"

func (m *DelayObj) Call(action string) bool {
	m.Action = action
	b, err := json.Marshal(m)
	if err != nil {
		return false
	}

	rds := DbHandler.GetRds(9)

	defer rds.Close()

	pf, _ := uuid.NewV7()
	tm := time.Now().Unix()

	taskId := "task_" + strconv.FormatInt(tm, 10)[2:] + strings.ReplaceAll(pf.String(), "-", "")

	str := `if redis.call('zadd',ARGV[1],ARGV[3],ARGV[2])==1 then return redis.call('set',ARGV[2],ARGV[4]); end`

	var getScript = redis.NewScript(0, str)

	//-- 解锁
	r1, err := getScript.Do(rds, DEFAULT_QUEUE_NAME, taskId, int(tm)+m.tm, string(b))

	if v, ok := r1.(string); ok && v == "OK" {
		fmt.Println("create an async task:", taskId)
		return true
	}
	fmt.Println("create an async task fail:", r1, taskId)
	return false
}
