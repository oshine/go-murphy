package mdb

import (
	"database/sql"
	"fmt"
	"time"
)

func (m *DbManager) InitDb(db *Database) error {

	conStr := m.getLinkStr(db)

	dbHandler, err := sql.Open(db.Adapter, conStr)
	if err != nil {
		return err
	}

	m.Db = dbHandler

	init := 10
	maxc := 20
	to := 600

	dbHandler.SetConnMaxLifetime(time.Duration(to) * time.Second)
	dbHandler.SetMaxIdleConns(init)
	dbHandler.SetMaxOpenConns(maxc)
	err = dbHandler.Ping()

	if err != nil {
		return err
	}

	return nil

}

type DbResult struct {
	Key   []string `json:"key"`
	Value []any    `json:"value"`
}

func (m *DbManager) Begin(f func(tx *sql.Tx) error) error {
	begin, err := m.Db.Begin()
	if err != nil {
		return err
	}
	err = f(begin)
	if err != nil {
		begin.Rollback()
		return err
	}
	begin.Commit()
	return nil
}

func (m *DbManager) queryByIds(ids []string) ([]DbResult, error) {
	result := make([]DbResult, 0)
	for _, id := range ids {
		rows, err := m.Db.Query(fmt.Sprintf("select id from "+" where id=%d", id))
		if err != nil {
			return nil, err
		}
		defer rows.Close()
		for rows.Next() {

		}

	}
	return result, nil
}

func (m *DbManager) execDb(query string) (sql.Result, error) {

	return m.Db.Exec(query)
}

func (m *DbManager) queryTrs(query string) []*DbResult {

	rows, err := m.Db.Query(query)

	defer rows.Close()

	if err == nil {
		var result = make([]*DbResult, 0)
		for rows.Next() {
			//fb := fieldbinding.NewFieldBinding()
			cols, err := rows.Columns()
			if err != nil {
				fmt.Println("get sql columns err:", err.Error())
				continue
			}

			params := make([]interface{}, len(cols))
			for i := 0; i < len(cols); i++ {
				params[i] = &params[i]
			}

			if err := rows.Scan(params...); err != nil {
				fmt.Println("scan err:", err.Error())
			} else {

				result = append(result, &DbResult{cols, params})
			}
		}
		return result
	} else {
		fmt.Println("query sql err:", err.Error())
	}

	return nil
}

func (m *DbManager) queryId(query string) []int64 {

	rows, err := m.Db.Query(query)

	defer rows.Close()

	if err == nil {
		var result []int64
		for rows.Next() {
			//fb := fieldbinding.NewFieldBinding()
			//cols, err := rows.Columns()
			//if err != nil {
			//	fmt.Println("get sql columns err:", err.Error())
			//	continue
			//}

			var p int64

			if err := rows.Scan(&p); err != nil {
				fmt.Println("scan err:", err.Error())
			} else {

				result = append(result, p)
			}
		}
		return result
	} else {
		fmt.Println("query sql err:", err.Error())
	}

	return nil
}
