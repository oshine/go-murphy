package mdb

import (
	"database/sql"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

var TABLE_PREFIX string

const TABLE_ROW_CACHE = "_tbl_row_"

type IMode interface {
	GetObj() MField
}

type MField interface {
	GetMapField() map[string]string
}

type IPagination interface {
	SetDao(pf []any)
	SetCount(total, current, pageSize int64)
}

type Base struct {
	GetId         func() int64
	SetObjId      func(id int64)
	GetEmptyModel func() IMode
	GetSelfModel  func() IMode
	GetPInstance  func() IPagination

	Obj  MField
	Name string

	_where  string
	_order  string
	_limit  string
	_offset string

	NotCache bool

	fieldType map[string]string

	ChangeKey []string

	Tx *sql.Tx

	_p  int64
	_ps int64
}

func (b *Base) GetObj() MField {
	return b.Obj
}

func (b *Base) SetObjIdFun(f func(id int64)) {
	b.SetObjId = f
}

func (b *Base) SetGetIdFun(f func() int64) {
	b.GetId = f
}

func (b *Base) AppendChangeKey(field string) {
	b.ChangeKey = append(b.ChangeKey, field)
}

func (b *Base) SetName(name string) {
	b.Name = TABLE_PREFIX + name
}

func (b *Base) exec(sql string) (sql.Result, error) {
	if b.Tx != nil {
		res, err := b.Tx.Exec(sql)
		if err != nil {
			return nil, err
		}
		return res, nil
	} else {
		res, err := DbHandler.execDb(sql)
		if err != nil {
			return nil, err
		}
		return res, nil
	}
}

func (b *Base) Save() bool {

	id := b.GetId()

	mp := b.Obj.GetMapField() // b.GetSelfModel().GetObj().GetMapField()

	t := reflect.ValueOf(b.Obj).Elem()

	if id != 0 {
		set := ""
		for _, i2 := range b.ChangeKey {
			p := t.FieldByName(mp[i2])

			switch p.Kind() {
			case reflect.String:
				r := strings.ReplaceAll(p.String(), "\n", "\\\n")
				r = strings.ReplaceAll(r, "\"", "\\\"")

				set = set + ",`" + i2 + "`=\"" + r + "\""
			default:
				set = set + ",`" + i2 + "`=" + fmt.Sprintf("%v", p)
			}
		}

		usql := "update " + b.Name + " set " + set[1:] + " where id=" + strconv.FormatInt(id, 10)
		fmt.Println("update:" + usql)

		_, err := b.exec(usql)
		if err != nil {
			fmt.Println(err.Error())
			return false
		}

		key := TABLE_ROW_CACHE + b.Name + strconv.FormatInt(id, 10)

		rc := DbHandler.getRds0()
		rc.Do("expire", key, 0)
		rc.Close()
		b.ChangeKey = []string{}
	} else {
		set := ""
		val := ""
		for _, i2 := range b.ChangeKey {
			p := t.FieldByName(mp[i2])

			set += ",`" + i2 + "`"
			switch p.Kind() {
			case reflect.String:
				val += ",\"" + strings.ReplaceAll(fmt.Sprintf("%v", p), "\"", "\\\"") + "\""
			default:
				val += "," + fmt.Sprintf("%v", p)
			}

		}

		sb := strings.Builder{}
		sb.WriteString("insert into")
		sb.WriteString(" `" + b.Name + "` (" + set[1:] + ")VALUES(" + val[1:] + ")")

		fmt.Println("insert:" + sb.String())
		res, err := b.exec(sb.String())
		if err != nil {
			fmt.Println(err.Error())
			return false
		}

		id1, _ := res.LastInsertId()
		b.SetObjId(id1)

	}
	return true
}

func (b *Base) Delete() bool {
	id := b.GetId()
	if id != 0 {
		_, err := DbHandler.execDb("delete from " + b.Name + " where id=" + strconv.Itoa(int(id)))
		if err != nil {
			return false
		}
	}

	return true
}

func (b *Base) WithTx(tx *sql.Tx) {
	b.Tx = tx
}

func (b *Base) SetObj(p MField) int {
	b.Obj = p
	return 0
}
