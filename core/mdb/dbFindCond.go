package mdb

import (
	"fmt"
	"gitee.com/oshine/murphy/core/def"
	"strconv"
	"strings"
	"time"
)

func (b *Base) Where(where string, bind ...any) *Base {
	for _, v := range bind {

		var p string
		switch v.(type) {
		case string:
			p = "\"" + strings.ReplaceAll(v.(string), "\"", "\\\"") + "\""
		case bool:
			p = strconv.FormatBool(v.(bool))
		case time.Time:
			p = strconv.FormatInt(v.(time.Time).UnixMilli(), 10)
		default:
			p = fmt.Sprintf("%v", v)
		}

		where = strings.Replace(where, "{?}", p, 1)
	}

	b._where = where
	return b
}

// Order 排序，如：id desc
func (b *Base) Order(order string) *Base {
	b._order = "order by " + order
	return b
}

func (b *Base) Page(page int64) *Base {
	b._p = page
	b._ps = 20
	page = (page - 1) * 20
	b._limit = " limit 20" + " offset  " + strconv.Itoa(int(page))

	return b
}

func (b *Base) PageLimit(page, pageSize int64) *Base {
	if pageSize > 100 {
		println("Warning: In func PageLimit(), pageSize no more than 100")
	}
	b._p = page
	b._ps = pageSize
	page = (page - 1) * pageSize
	b._limit = " limit " + strconv.Itoa(int(pageSize)) + " offset " + strconv.Itoa(int(page))
	return b
}

func (b *Base) WithCond(y def.ICond) {
	str := y.Build()
	p1 := str[:len(str)-3]
	b._where = p1
}
