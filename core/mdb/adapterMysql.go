//go:build mysql || (!mysql && !postgres && !mssql)

package mdb

import "fmt"

func (m *DbManager) getLinkStr(db *Database) string {

	// mysql "user:password@()/dbname?charset=utf8&parseTime=True&loc=Local
	// postgres host=myhost port=myport user=gorm dbname=gorm password=mypassword

	return fmt.Sprintf("%s:%s@(%s:%s)/%s?parseTime=True&loc=Local", db.User, db.Pass, db.Host, db.Port, db.Db)
}
