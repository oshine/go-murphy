package mdb

import (
	"fmt"
	"github.com/goccy/go-json"
	"log"
	"reflect"
	"strconv"
)

func (b *Base) _findByIds() []any {
	if b._limit == "" {
		b._limit = " limit 0,20 "
		println("must set page and pagesize.")
	}
	var sql = "select id from "
	if b._where != "" {
		sql += b.Name + " where " + b._where + " " + b._order + " " + b._limit
	} else {
		sql += b.Name + " " + b._order + " " + b._limit
	}

	fmt.Println(sql)

	result := DbHandler.queryId(sql)
	if result == nil {
		return nil
	}

	return b.getAllModels(result)
}

func (b *Base) _findMulti(id string) (pr1 []any, result []*DbResult) {
	sql := "select * from " + b.Name + " where id in (" + id + ") " + b._order

	fmt.Println(sql)

	result = DbHandler.queryTrs(sql)

	if len(result) > 0 {
		for _, v := range result {

			var m = b.GetEmptyModel()
			f := m.GetObj()
			mp := f.GetMapField()
			tf := reflect.ValueOf(f).Elem()

			for i := 0; i < len(v.Key); i++ {
				if pk, ok := mp[v.Key[i]]; ok {
					field := tf.FieldByName(pk)

					if field.IsValid() {
						var p any
						switch po := v.Value[i]; po.(type) {
						case []uint8:
							if field.Kind() == reflect.Float64 {
								p, _ = strconv.ParseFloat(string(po.([]uint8)), 64)
								v.Value[i] = p
							} else {
								p = string(po.([]uint8))
								v.Value[i] = p
							}
						case nil:
							continue
						default:
							p = po
						}
						field.Set(reflect.ValueOf(p))
					}
				}
			}

			pr1 = append(pr1, m)

		}

	}

	return
}

// json转model
func (b *Base) _toModel(p any) any {
	var m IMode
	m = b.GetEmptyModel()

	f := m.GetObj()
	mp := f.GetMapField()

	tf := reflect.ValueOf(f).Elem()
	v := p.(*DbResult)
	key := v.Key
	value := v.Value

	for i, vk := range key {

		if pk, ok := mp[vk]; ok {
			field := tf.FieldByName(pk)
			if field.IsValid() {
				var pv any
				po := value[i]
				if po == nil {
					continue
				}
				switch field.Interface().(type) {
				case int64:
					if po != nil {
						pv = int64(po.(float64))

					} else {
						continue
					}
				default:
					pv = po
				}
				field.Set(reflect.ValueOf(pv))
			}
		}

	}

	return m
}

func (b *Base) getAllModels(ids []int64) []any {

	var idsStr string
	if b.NotCache {

		var result = make([]interface{}, len(ids))

		for _, id := range ids {
			idsStr += "," + strconv.FormatInt(id, 10)
		}
		df, _ := b._findMulti(idsStr[1:])

		for ind, d := range df {
			result[ind] = d
		}
		return result
	}

	rc := DbHandler.getRds0()
	defer rc.Close()

	name := TABLE_ROW_CACHE + b.Name
	var result = make([]interface{}, len(ids))
	var notFindIds = make(map[int64]int)
	for ind, id := range ids {
		strId := name + strconv.FormatInt(id, 10)

		res, err := rc.Do("get", strId)
		if res == nil {
			// not find on redis cache
			// find it in database

			notFindIds[id] = ind
			idsStr += "," + strconv.FormatInt(id, 10)

			continue
		}

		var v *DbResult
		err = json.Unmarshal(res.([]uint8), &v)
		if err != nil {
			log.Println("get data from redis err", err.Error())
			return nil
		}

		result[ind] = b._toModel(v)

	}

	// 开始查询数据库，没有在缓存中找到的数据

	if len(idsStr) != 0 {
		df, rd := b._findMulti(idsStr[1:])

		for i, d := range df {

			var id = rd[i].Value[0].(int64)

			r, err := json.Marshal(rd[i])

			if err != nil {
				fmt.Println(err.Error())
				return nil
			}

			strId := name + strconv.FormatInt(id, 10)
			err = rc.Send("setex", strId, 3600, string(r))
			if err != nil {
				fmt.Println(err.Error())
			}

			//mo := b._toModel(d)

			result[notFindIds[id]] = d

		}

		rc.Flush()

	}

	return result
}

func (b *Base) _findCount() int64 {
	sql := "select count(id) from " + b.Name
	if b._where != "" {
		sql += " where " + b._where + " "
	}
	fmt.Println(sql)

	result := DbHandler.queryId(sql)
	if result == nil {
		return 0
	}
	if len(result) > 0 {
		return result[0]
	}
	return 0
}

func (b *Base) FindPagination(isCount bool) any {
	pag := b.GetPInstance()

	if isCount {
		total := b._findCount()

		pag.SetCount(total, b._p, b._ps)
	}

	p := b._findByIds()
	if len(p) > 0 {
		pag.SetDao(p)
	}

	return pag
}
