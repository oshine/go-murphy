package mdb

import (
	"github.com/gomodule/redigo/redis"
	"time"
)

// 数据库缓存专用
func (m *DbManager) getRds0() redis.Conn {
	r := m.Rdb.Get()
	r.Do("SELECT", 0)
	return r
}

func (m *DbManager) GetSsdb0() redis.Conn {
	return m.Ssdb.Get()
}

func (m *DbManager) GetRds1() redis.Conn {
	r := m.Rdb.Get()
	r.Do("SELECT", 1)
	return r
}

func (m *DbManager) GetRds2() redis.Conn {
	r := m.Rdb.Get()
	r.Do("SELECT", 2)
	return r
}

func (m *DbManager) GetRds3() redis.Conn {
	r := m.Rdb.Get()
	r.Do("SELECT", 3)
	return r
}

func (m *DbManager) GetRds4() redis.Conn {
	r := m.Rdb.Get()
	r.Do("SELECT", 4)
	return r
}

func (m *DbManager) GetRds(db int) redis.Conn {
	r := m.Rdb.Get()
	r.Do("SELECT", db)
	return r
}

func (m *DbManager) InitSsdb(conf *Redis) bool {

	// 建立连接池
	rdb := &redis.Pool{
		// 从配置文件获取maxidle以及maxactive，取不到则用后面的默认值
		MaxIdle:   int(conf.Init),   //最初的连接数量
		MaxActive: int(conf.MaxCon), //最大连接数量
		// MaxActive:   0,                 //连接池最大连接数量,不确定可以用0（0表示自动定义），按需分配
		IdleTimeout: time.Duration(conf.Idle) * time.Second, //连接关闭时间 300秒 （300秒不使用自动关闭）
		Dial: func() (redis.Conn, error) { //要连接的redis数据库

			c, err := redis.Dial("tcp", conf.Host+":"+conf.Port)
			if err != nil {
				println(err.Error())
				return nil, err
			}
			if conf.Psw != "" {
				if _, err := c.Do("AUTH", conf.Psw); err != nil {
					_ = c.Close()
					println(err.Error())
					return nil, err
				}
			}

			//if conf.Db != "" {
			//	if _, err := c.Do("SELECT", conf.Db); err != nil {
			//		c.Close()
			//		println(err.Error())
			//		return nil, err
			//	}
			//}

			return c, nil
		},
	}
	_, err := rdb.Get().Do("ping")
	if err != nil {
		return false
	}
	m.Ssdb = rdb
	return true
}

func (m *DbManager) InitRds(conf *Redis) bool {

	//addr := "127.0.0.1"
	//psw := ""
	//Db := "0"
	//init := 10
	//maxCon := 60
	//idle := 100

	// 建立连接池
	rdb := &redis.Pool{
		// 从配置文件获取maxidle以及maxactive，取不到则用后面的默认值
		MaxIdle:   int(conf.Init),   //最初的连接数量
		MaxActive: int(conf.MaxCon), //最大连接数量
		// MaxActive:   0,                 //连接池最大连接数量,不确定可以用0（0表示自动定义），按需分配
		IdleTimeout: time.Duration(conf.Idle) * time.Second, //连接关闭时间 300秒 （300秒不使用自动关闭）
		Dial: func() (redis.Conn, error) { //要连接的redis数据库

			c, err := redis.Dial("tcp", conf.Host+":"+conf.Port)
			if err != nil {
				println(err.Error())
				return nil, err
			}
			if conf.Psw != "" {
				if _, err := c.Do("AUTH", conf.Psw); err != nil {
					_ = c.Close()
					println(err.Error())
					return nil, err
				}
			}
			if conf.Db != "" {
				if _, err := c.Do("SELECT", conf.Db); err != nil {
					c.Close()
					println(err.Error())
					return nil, err
				}
			}

			return c, nil
		},
	}

	_, err := rdb.Get().Do("ping")
	if err != nil {
		return false
	}

	m.Rdb = rdb

	return true
}
