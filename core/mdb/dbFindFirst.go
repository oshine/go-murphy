package mdb

import (
	"fmt"
	"github.com/goccy/go-json"
	"reflect"
	"strconv"
	"strings"
)

// 从数据库中查找新的数据
func (b *Base) _findSelfOne(id int64) (bool, []*DbResult) {
	sql := "select * from " + b.Name + " where id=" + strconv.FormatInt(id, 10) + " " + b._order + " limit 1"

	fmt.Println(sql)

	result := DbHandler.queryTrs(sql)

	if len(result) > 0 {
		v := result[0]
		var m IMode

		m = b.GetSelfModel()

		f := m.GetObj()
		mp := f.GetMapField()

		if len(mp) != len(v.Key) {
			// 数据库长度和本地类对不上
			// return false, nil
		}

		var (
			pk string
			ok bool
		)

		tf := reflect.ValueOf(f).Elem()
		for i := 0; i < len(v.Key); i++ {

			if pk, ok = mp[v.Key[i]]; ok {
				field := tf.FieldByName(pk)

				if field.IsValid() {

					var p any
					switch po := v.Value[i]; po.(type) {
					case []uint8:
						if field.Kind() == reflect.Float64 {
							p, _ = strconv.ParseFloat(string(po.([]uint8)), 64)
							v.Value[i] = p
						} else {
							p = string(po.([]uint8))
							v.Value[i] = p
						}

					case nil:
						continue
					default:
						p = po
					}
					field.Set(reflect.ValueOf(p))
				}
			}

		}
		return true, result
	}
	return false, nil
}

// json转model
func (b *Base) _toSelfModel(p any) bool {
	var m = b.GetSelfModel()

	f := m.GetObj()
	mp := f.GetMapField()

	tf := reflect.ValueOf(f).Elem()
	v := p.(*DbResult)

	key := v.Key
	value := v.Value

	var (
		pk string
		ok bool
	)

	for i, vk := range key {

		// json数据有的字段 而dao中没有，可能是新版本导致的，需要判断当前版本中是否存在这个字段后才能设置
		if pk, ok = mp[vk]; ok {
			field := tf.FieldByName(pk)
			if field.IsValid() {
				var pv any
				po := value[i]
				if po == nil {
					continue
				}
				switch field.Interface().(type) {
				case int64:
					if po != nil {
						pv = int64(po.(float64))

					} else {
						continue
					}
				default:
					pv = po
				}

				field.Set(reflect.ValueOf(pv))
			}
		}

	}

	return true
}

func (b *Base) toSelfObj(id int64) bool {
	if b.NotCache {
		df, _ := b._findSelfOne(id)
		return df
	}

	rc := DbHandler.getRds0()

	strId := TABLE_ROW_CACHE + b.Name + strconv.FormatInt(id, 10)

	res, err := rc.Do("get", strId)
	if res == nil {
		// not find on redis cache
		// find it in database

		df, rd := b._findSelfOne(id)

		go func() {
			defer rc.Close()

			if df {
				r, err := json.Marshal(rd[0])
				if err != nil {
					fmt.Println(err.Error())
				}
				_, err = rc.Do("setex", strId, 3600, string(r))
				rc.Close()
				if err != nil {
					fmt.Println(err.Error())
				}
			}
		}()

		return df
	}

	rc.Close()

	var v *DbResult
	err = json.Unmarshal(res.([]uint8), &v)
	if err != nil {
		return false
	}

	return b._toSelfModel(v)
}

func (b *Base) _findId() int64 {
	var sql = "select id from "
	if b._where != "" {
		sql += b.Name + " where " + b._where + " " + b._order + " limit 1"
	} else {
		sql += b.Name + " " + b._order + " limit 1"
	}

	fmt.Println(sql)

	r := DbHandler.queryId(sql)
	if r == nil {
		return 0
	}
	if len(r) > 0 {
		return r[0]
	}

	return 0
}

func (b *Base) FindFirst() bool {
	p := b._findId()
	if p > 0 {
		return b.toSelfObj(p)
	}
	return false
}

func (b *Base) Exist() bool {
	return b._findId() > 0
}

func (b *Base) ExistBy(key string, val any) bool {
	if key == "id" {
		var p int64
		switch val.(type) {
		case int64:
			p = val.(int64)
		case string:
			var err error
			p, err = strconv.ParseInt(val.(string), 10, 64)
			if err != nil {
				return false
			}
		case int:
			p = int64(val.(int))
		default:
			return false
		}

		b._where = key + "=" + strconv.FormatInt(p, 10)

	} else {
		switch val.(type) {
		case string:
			b._where = key + "=\"" + strings.ReplaceAll(val.(string), "\"", "\\\"") + "\""
		default:
			b._where = key + "=" + fmt.Sprintf("%v", val)
		}
	}

	return b._findId() > 0
}

func (b *Base) FindFirstBy(key string, val any) bool {
	if key == "id" {
		var p int64
		switch val.(type) {
		case int64:
			p = val.(int64)
		case string:
			var err error
			p, err = strconv.ParseInt(val.(string), 10, 64)
			if err != nil {
				return false
			}
		case int:
			p = int64(val.(int))
		default:
			return false
		}

		return b.toSelfObj(p)
	}

	switch val.(type) {
	case string:
		b._where = key + "=\"" + strings.ReplaceAll(val.(string), "\"", "\\\"") + "\""
	default:
		b._where = key + "=" + fmt.Sprintf("%v", val)
	}

	return b.FindFirst()
}
