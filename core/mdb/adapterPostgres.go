//go:build postgres

package mdb

func (m *DbManager) getLinkStr() string {

	// mysql "user:password@/dbname?charset=utf8&parseTime=True&loc=Local
	// postgres host=myhost port=myport user=gorm dbname=gorm password=mypassword
	return "user:password@/dbname?charset=utf8&parseTime=True&loc=Local"
}
