package tm

import "time"

func BeginDay(t int) int {
	return 0
}

func EndOfDay(t int) int {
	return 0
}

func Unix() int64 {
	return time.Now().Unix()
}
