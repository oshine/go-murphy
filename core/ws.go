package core

import (
	"fmt"
	"gitee.com/oshine/murphy/core/def"
	"github.com/fasthttp-contrib/websocket"
	"github.com/goccy/go-json"
	"github.com/valyala/fasthttp"
	"runtime/debug"
)

type Ws struct {
	ctx *fasthttp.RequestCtx
}

func (ws *Ws) InitWebsocket(ctx *fasthttp.RequestCtx) {
	ws.ctx = ctx
}

func (ws *Ws) Start() {

}

type WsRouter struct {
	path string
	len  int
	fun  func(c *websocket.Conn) error
}
type SvrWs struct {
	svrConfig  *fasthttp.Server
	groupIndex []string
	routers    []*WsRouter
	SvrType    string
}

func (ws *SvrWs) Router(path string, f func(c *websocket.Conn) error) *SvrWs {
	ws.routers = append(ws.routers, &WsRouter{
		path: path,
		fun:  f,
	})

	return ws
}

func (ws *SvrWs) MiddleWare(f func(c *websocket.Conn) error) *SvrWs {

	return &SvrWs{}
}

type WsMsg struct {
	Op   string
	Msg  string
	Data interface{}
}

func (m *WsMsg) Params(key string) any {
	return m.Data.(def.MapStrAny)[key]
}

type WsCon struct {
	C *websocket.Conn
}

func (w *WsCon) Send(v any) error {

	b, err := json.Marshal(v)
	if err != nil {
		return err
	}

	return w.C.WriteMessage(websocket.TextMessage, b)
}

func (w *WsCon) Read(v *WsMsg) bool {

	mt, message, err := w.C.ReadMessage()
	if err != nil {
		fmt.Println(err)
		return false
	}
	if mt != websocket.TextMessage {
		return false
	}

	err = json.Unmarshal(message, &v)
	if err != nil {
		err = w.C.WriteMessage(mt, []byte(err.Error()))
		return false
	}

	return true

}

func (ws *SvrWs) RunTLS(addr, cert, key string) {
	hl := func(ctx *fasthttp.RequestCtx) {

		var uri = string(ctx.Path())
		fmt.Println(uri)

		for _, router := range ws.routers {
			if uri == router.path {

				var upgrade = websocket.New(func(c *websocket.Conn) {
					defer func() {
						defer c.Close()
						if err := recover(); err != nil {
							fmt.Println(err)
							debug.PrintStack()
						}
					}()

					err := router.fun(c)
					if err != nil {
						return
					}
				})

				err := upgrade.Upgrade(ctx)
				if err != nil {
					fmt.Println(err)
					return
				}

				break
			}
		}
	}

	if ws.svrConfig == nil {
		ws.svrConfig = &fasthttp.Server{
			Handler: hl, Name: "MURPHY SERVER",
			MaxRequestBodySize: 10000 * 1024 * 1024,
		}
	} else {
		ws.svrConfig.Handler = hl
	}

	fmt.Println("Websocket Server Listening on ws://" + addr)

	var err error
	if cert != "" && key != "" {
		err = ws.svrConfig.ListenAndServeTLS(addr, cert, key)
	} else {
		err = ws.svrConfig.ListenAndServe(addr)
	}

	fmt.Println(err)

}
