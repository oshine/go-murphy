package core

import (
	"cmp"
	"fmt"
	"gitee.com/oshine/murphy/core/text"
	"github.com/olekukonko/tablewriter"
	"log"
	"os"
	"reflect"
	"runtime/debug"
	"slices"
	"strings"
)
import "github.com/valyala/fasthttp"

type Router struct {
	path     string
	action   string
	l        string
	r        string
	Val      reflect.Value
	Instance any
	fun      func(ctx *fasthttp.RequestCtx) error
	authFun  reflect.Value

	Sg *SvrGroup
}

func (r *Router) Call(p1 *string, ctx *fasthttp.RequestCtx) {
	p := *p1
	sg := r.Sg

	// run middleware
	if len(r.Sg.midw) > 0 {
		for _, mr := range sg.midw {
			if !mr(ctx, sg.prefix, p) {
				fmt.Println("middleware auth fail ", sg.prefix, p)
				return
			}
		}
	}

	var (
		in, res []reflect.Value
	)

	// auth鉴权
	if r.authFun.IsValid() {
		in = []reflect.Value{
			reflect.ValueOf(r.Instance),
			reflect.ValueOf(ctx),
			reflect.ValueOf(r.l),
			reflect.ValueOf(r.r),
		}
		res = r.authFun.Call(in)
		if res != nil {
			var el = res[0]
			if !el.Bool() {
				fmt.Println(r.action+" auth fail,", el)
				return
			}
		}
	}

	in = []reflect.Value{
		reflect.ValueOf(r.Instance),
		reflect.ValueOf(ctx),
	}
	res = r.Val.Call(in)
	if res != nil {
		var el = res[0].Elem()
		if el.Kind() != reflect.Invalid {
			fmt.Println("call:"+r.action+" error,", el)
		}
	}
}

func (s *SvrHttp) Run(addr string) {
	s.RunTLS(addr, "", "")
}

func (s *SvrHttp) WithServerConfig(config *fasthttp.Server) *SvrHttp {
	s.svrConfig = config
	return s
}

type Call struct {
	call func(f *fasthttp.RequestCtx) error
}

func (s *SvrHttp) RunTLS(addr, cert, key string) {
	hl := func(ctx *fasthttp.RequestCtx) {

		defer func() {
			if err := recover(); err != nil {
				fmt.Println(err)
				debug.PrintStack()
			}
		}()

		var uri = string(ctx.Path())

		// fmt.Println("uri:" + string(uri))

		if uri == "/" && s.Root != nil {
			s.Root.Call(&uri, ctx)
			return
		}

		for _, sg := range s.group {
			if len(uri) >= sg.len && uri[:sg.len] == sg.prefix {
				for _, r := range sg.router {
					var p = uri[sg.len:]
					if p == "/"+r.action {
						r.Call(&p, ctx)
					}
				}

				return
			}
		}

		ctx.SetBodyString("not found")

	}

	// hl = fasthttp.CompressHandler(hl)

	if s.svrConfig == nil {
		s.svrConfig = &fasthttp.Server{
			Handler: hl, Name: "MURPHY SERVER",
			MaxRequestBodySize: 10000 * 1024 * 1024,
		}
	} else {
		s.svrConfig.Handler = hl
	}

	fmt.Println("Http Server Listening on http://" + addr)

	var err error
	if cert != "" && key != "" {
		err = s.svrConfig.ListenAndServeTLS(addr, cert, key)
	} else {
		err = s.svrConfig.ListenAndServe(addr)
	}

	fmt.Println(err)
}

type Server struct {
}

type SvrHttp struct {
	svrConfig  *fasthttp.Server
	groupIndex []string
	group      []*SvrGroup
	SvrType    string

	Root *Router
}

func (s *Server) Http() *SvrHttp {
	return &SvrHttp{}
}

func (s *Server) Ws() *SvrWs {
	return &SvrWs{}
}

func (s *SvrHttp) Group(prefix string, f func(group *SvrGroup)) *SvrHttp {
	if len(prefix) > 0 && prefix[0] != '/' {
		prefix = "/" + prefix
	}
	if prefix == "/" {
		prefix = ""
	}

	s.groupIndex = append(s.groupIndex, prefix)

	group := &SvrGroup{prefix: prefix, len: len(prefix), svr: s}
	f(group)

	s.group = append(s.group, group)
	return s
}

type MiddleWare struct {
	fun func(ctx *fasthttp.RequestCtx) error
}

type SvrGroup struct {
	prefix string
	len    int
	auth   string
	svr    *SvrHttp
	router []*Router
	midw   []func(ctx *fasthttp.RequestCtx, prefix, action string) bool
	home   string
}

type Controller interface {
	func(ctx *fasthttp.RequestCtx) error
}

func (sg *SvrGroup) Bind(c1 ...any) {
	if sg.auth == "" {
		//sg.auth = "Auth"
	}

	var pTable [][]string
	var temp []string

	for _, c := range c1 {
		ele := reflect.TypeOf(c)

		var authMethod reflect.Method

		if sg.auth != "" {
			authMethod, _ = ele.MethodByName(sg.auth)
			if authMethod.Func.IsValid() == false {
				log.Println("Auth Method not find")
				os.Exit(0)
			} else {
				// , prefix, action string
				tp := authMethod.Func.Type()
				if tp.NumOut() != 1 || tp.Out(0).String() != "bool" ||
					tp.NumIn() != 4 || tp.In(1).String() != "*fasthttp.RequestCtx" ||
					tp.In(2).String() != "string" || tp.In(3).String() != "string" {
					log.Println("Auth Method type is \"func (ctx *fasthttp.RequestCtx, name, action string) bool\"")
					os.Exit(0)
				}
			}
		}

		for i := 0; i < ele.NumMethod(); i++ {
			me := ele.Method(i)
			if me.IsExported() {
				if me.Name == "Init" {
					if me.Func.Type().NumIn() != 1 || me.Func.Type().NumOut() != 0 {
						println("Init func in " + ele.Elem().Name() + " is not correct")
					} else {
						in := []reflect.Value{
							reflect.ValueOf(c),
						}
						me.Func.Call(in)
					}
					continue
				}
				if me.Func.Type().NumOut() != 1 || me.Func.Type().Out(0).String() != "error" {
					continue
				}
				if me.Func.Type().NumIn() != 2 || me.Func.Type().In(1).String() != "*fasthttp.RequestCtx" {
					continue
				}
				if me.Func.Type().NumIn() > 0 {

					var pf = text.Snack(ele.Elem().Name())
					pf = strings.Replace(pf, "_controller", "", 1)

					n := text.Snack(me.Name)
					act := pf + "/" + n

					if strings.TrimLeft(sg.home, "/") == act {
						pr := &Router{
							authFun:  authMethod.Func,
							action:   "/",
							Val:      me.Func,
							Instance: c,
							Sg:       sg,
							l:        pf,
							r:        n,
						}
						sg.router = append(sg.router, pr)
						sg.svr.Root = pr

						temp = []string{"/", ele.Elem().PkgPath(), ele.Elem().Name(), me.Name}

					}

					pTable = append(pTable, []string{"" + sg.prefix + "/" + act, ele.Elem().PkgPath(), ele.Elem().Name(), me.Name})

					sg.router = append(sg.router, &Router{
						authFun:  authMethod.Func,
						action:   act,
						Val:      me.Func,
						Instance: c,
						Sg:       sg,
						l:        pf,
						r:        n,
					})
				}
			}
		}

	}

	pTable = append([][]string{temp}, pTable...)

	printTable(pTable)
}

func printTable(t [][]string) {

	slices.SortFunc(t, func(a, b []string) int {
		return cmp.Compare(strings.ToLower(a[0]), strings.ToLower(b[0]))
	})

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Api", "Folder", "Controller", "Method"})

	table.SetHeaderColor(tablewriter.Colors{tablewriter.Bold, tablewriter.BgBlackColor},
		tablewriter.Colors{tablewriter.FgHiRedColor, tablewriter.Bold, tablewriter.BgBlackColor},
		tablewriter.Colors{tablewriter.BgBlackColor, tablewriter.Bold, tablewriter.FgHiBlueColor},
		tablewriter.Colors{tablewriter.BgBlackColor, tablewriter.Bold, tablewriter.FgWhiteColor})

	table.SetColumnColor(tablewriter.Colors{tablewriter.Bold, tablewriter.FgGreenColor},
		tablewriter.Colors{tablewriter.Bold, tablewriter.FgRedColor},
		tablewriter.Colors{tablewriter.Bold, tablewriter.FgHiBlueColor},
		tablewriter.Colors{tablewriter.FgCyanColor, tablewriter.Bold})

	table.AppendBulk(t)
	table.Render()
}

func (sg *SvrGroup) Auth(s string) {
	sg.auth = s
}

// Home Set home uri /
func (sg *SvrGroup) Home(s string) {
	sg.home = s
}
