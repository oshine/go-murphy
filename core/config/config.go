package config

import (
	"gitee.com/oshine/murphy/core/text"
	"github.com/joho/godotenv"
	"os"
	"reflect"
	"strconv"
	"strings"
)

func LoadEnv() error {
	return godotenv.Load()
}

func FromEnv(v any) {

	val := reflect.ValueOf(v).Elem()

	for i := 0; i < val.NumField(); i++ {
		f := val.Field(i)
		dc := f.String()

		var res string
		var t string
		if len(dc) > 0 && dc[0] == '@' {
			p := strings.Trim(dc, " \t")
			if strings.Index(p, " ") == -1 {
				t = p[1:]
			} else {
				t = text.SubEx("n", dc[1:], " ")
			}

			host := os.Getenv(t)
			if host == "" {
				sp := strings.Split(dc, " ")
				res = sp[1]
			} else {
				res = host
			}
		} else {
			continue
		}

		switch f.Kind() {
		case reflect.String:
			f.SetString(res)
		case reflect.Int:
			i, err := strconv.Atoi(res)
			if err != nil {
				panic(err)
			}
			f.SetInt(int64(i))
		case reflect.Bool:

			b, err := strconv.ParseBool(res)
			if err != nil {
				panic(err)
			}
			f.SetBool(b)
		case reflect.Int64:
			b, err := strconv.ParseInt(res, 10, 64)
			if err != nil {
				panic(err)
			}
			f.SetInt(b)
		case reflect.Float64:
			b, err := strconv.ParseFloat(res, 10)
			if err != nil {
				panic(err)
			}
			f.SetFloat(b)
		}
	}

}
