package text

import "os"
import "gopkg.in/yaml.v3"

func YamlUnMarshal(f string ,v any) (  error) {
	b,err:=os.ReadFile(f)
	if err!=nil{
		return   err
	}
	return  yaml.Unmarshal(b,v)
}
