package text

import (
	"strings"
	"unicode"
)

func Snack(p string) string {
	var output []rune
	for i, r := range p {
		if i == 0 {
			output = append(output, unicode.ToLower(r))
		} else {
			if unicode.IsUpper(r) {
				output = append(output, '_')
			}

			output = append(output, unicode.ToLower(r))
		}
	}
	return string(output)
}

// Equal reports whether `a` and `b`, interpreted as UTF-8 strings,
// are equal under Unicode case-folding, case-insensitively.
func Equal(a, b string) bool {
	return strings.EqualFold(a, b)
}

// SearchArray searches string `s` in string slice `a` case-sensitively,
// returns its index in `a`.
// If `s` is not found in `a`, it returns -1.
func SearchArray(a []string, s string) int {
	for i, v := range a {
		if s == v {
			return i
		}
	}
	return NotFoundIndex
}

// InArray checks whether string `s` in slice `a`.
func InArray(s string, a []string) bool {
	return SearchArray(a, s) != NotFoundIndex
}

// PrefixArray adds `prefix` string for each item of `array`.
//
// Example:
// PrefixArray(["a","b"], "gf_") -> ["gf_a", "gf_b"]
func PrefixArray(array []string, prefix string) {
	for k, v := range array {
		array[k] = prefix + v
	}
}

const (
	TEXT_SUB_NORMAL = byte(iota)
	TEXT_SUB_NORMAL_TO_END
	TEXT_SUB_REVERT
	TEXT_SUB_REVERT_TO_END
)

func SubEx(mode byte, origin string, find string) string {

	switch mode {
	case TEXT_SUB_NORMAL:
		i := strings.Index(origin, find)
		if i == -1 {
			return ""
		}
		return origin[:i]
	case TEXT_SUB_NORMAL_TO_END:
		i := strings.LastIndex(origin, find)
		if i == -1 {
			return ""
		}
		return origin[:i]
	case TEXT_SUB_REVERT:
		i := strings.LastIndex(origin, find)
		if i == -1 {
			return ""
		}
		return origin[i+1:]
	case TEXT_SUB_REVERT_TO_END:
		i := strings.Index(origin, find)
		if i == -1 {
			return ""
		}
		return origin[i+1:]
	}
	return ""
}
