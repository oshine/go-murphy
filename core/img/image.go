package img

import (
	"github.com/nfnt/resize"
	"image"
	_ "image/gif"
	"image/jpeg"
	_ "image/jpeg"
	_ "image/png"
	"log"
	"os"
)

// ResizeFile 等比例缩放
func ResizeFile(filePath string, maxSize, quality int) (image.Image, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return ResizeToFile(f, maxSize, quality)
}

// ResizeImage 等比例缩放
func ResizeImage(img image.Image, maxSize int) (image.Image, error) {

	size := img.Bounds()
	var (
		w = size.Max.X
		h = size.Max.Y
	)

	var m1 image.Image
	var l uint
	if w > h {
		if w > maxSize {
			l = uint(maxSize)
		} else {
			l = uint(w)
		}

		m1 = resize.Resize(l, 0, img, resize.Lanczos3)

	} else {
		if h > maxSize {
			l = uint(maxSize)
		} else {
			l = uint(h)
		}

		m1 = resize.Resize(0, l, img, resize.Lanczos3)
	}

	return m1, nil
}

// ResizeToFile 等比例缩放
func ResizeToFile(f *os.File, maxSize, quality int) (image.Image, error) {
	decode, s, err := image.Decode(f)
	log.Println(s)
	if err != nil {
		return nil, err
	}
	m1, err := ResizeImage(decode, maxSize)
	if err != nil {
		return nil, err
	}
	f.Seek(0, 0)
	err = f.Truncate(0)
	if err != nil {
		return nil, err
	}

	err = jpeg.Encode(f, m1, &jpeg.Options{Quality: quality})
	if err != nil {
		return nil, err
	}

	return m1, nil
}
