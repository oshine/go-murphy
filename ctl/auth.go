package ctl

import (
	"github.com/goccy/go-json"
	"github.com/valyala/fasthttp"
)

func (m *AuthController) SetSkipRule(r *[][]string) {
	m._skipRule = r
}

type AuthController struct {
	Ctx
	CheckAuth func(ctx *fasthttp.RequestCtx) bool
	BeforeReq func(ctx *fasthttp.RequestCtx) bool
	_skipRule *[][]string
}

// Auth 所有控制层的鉴权方法
// 返回 true 表示成功
func (m *AuthController) Auth(ctx *fasthttp.RequestCtx, name, action string) bool {
	if !m.BeforeReq(ctx) {
		return false
	}
	if m._skipRule != nil {
		for _, i2 := range *m._skipRule {
			if i2[0] == name {
				for j := 1; j < len(i2); j++ {
					if action == i2[j] {
						return true
					}
				}
				break
			}
		}
	}

	if m.CheckAuth(ctx) {
		return true
	}

	b, _ := json.Marshal(&Msg{
		Code: -1,
		Msg:  "auth fail",
	})

	ctx.Response.Header.Set("Content-Type", "application/json")
	ctx.Response.SetBody(b)

	return false
}
