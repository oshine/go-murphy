package ctl

import (
	"github.com/goccy/go-json"
	"github.com/golang-jwt/jwt"
	"github.com/valyala/fasthttp"
	"time"
)

type Claims struct {
	ID       int64
	Username string `json:"username"`
	jwt.StandardClaims
}

type JwtController struct {
	Ctx
	signKey   []byte
	AuthOk    func(id *int64) bool
	_skipRule *[][]string
}

func GenerateToken(id int64, expire int, signKey []byte) (string, error) {
	nowTime := time.Now()
	expireTime := nowTime.Add(time.Duration(expire) * time.Second)
	issuer := "murphy"
	claims := Claims{
		ID: id,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expireTime.Unix(),
			Issuer:    issuer,
		},
	}
	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString(signKey)
	return token, err
}

func (m *JwtController) InitJwt(signKey []byte) {
	m.signKey = signKey
}

func (m *JwtController) SetSkipRule(r *[][]string) {
	m._skipRule = r
}

func ParseToken(token *string, signKey []byte) (*Claims, error) {
	tokenClaims, err := jwt.ParseWithClaims(*token, &Claims{},
		func(token *jwt.Token) (interface{}, error) {
			return signKey, nil
		})
	if err != nil {
		return nil, err
	}

	if tokenClaims != nil {
		if claims, ok := tokenClaims.Claims.(*Claims); ok && tokenClaims.Valid {
			return claims, nil
		}
	}

	return nil, err
}

// Auth 所有控制层的鉴权方法
// 返回 true 表示成功
func (m *JwtController) Auth(ctx *fasthttp.RequestCtx, name, action string) bool {
	if m._skipRule != nil {
		for _, i2 := range *m._skipRule {
			if i2[0] == name {
				for j := 1; j < len(i2); j++ {
					if action == i2[j] {
						return true
					}
				}
				break
			}
		}
	}

	token := string(ctx.Request.Header.Peek("token"))
	if len(token) != 0 {
		if r, err := ParseToken(&token, m.signKey); err == nil {
			m.AuthOk(&r.ID)
			return true
		}
	}

	b, _ := json.Marshal(&Msg{
		Code: -1,
		Msg:  "auth fail",
	})

	ctx.Response.Header.Set("Content-Type", "application/json")
	ctx.Response.SetBody(b)

	return false
}
