package ctl

import (
	"fmt"
	"gitee.com/oshine/murphy/core/encrypt"
	"gitee.com/oshine/murphy/core/tm"
	"github.com/goccy/go-json"
	"github.com/valyala/fasthttp"
	"strconv"
)

type IMember interface {
	CheckSid(id *int64, sid *string) bool
}

func GenerateSid(id *int64, key string) string {
	return fmt.Sprintf("%v.d%v", *id, encrypt.Sha1([]byte(key+strconv.Itoa(int(tm.Unix())))))
}

type SidController struct {
	AuthController
	CheckAuth func(sid *string, ctx *fasthttp.RequestCtx) bool
}

// Auth 所有控制层的鉴权方法
// 返回 true 表示成功
func (m *SidController) Auth(ctx *fasthttp.RequestCtx, name, action string) bool {
	if !m.BeforeReq(ctx) {
		return false
	}
	if m._skipRule != nil {
		for _, i2 := range *m._skipRule {
			if i2[0] == name {
				for j := 1; j < len(i2); j++ {
					if action == i2[j] {
						return true
					}
				}
				break
			}
		}
	}

	sid := string(ctx.Request.Header.Peek("sid"))
	if len(sid) != 0 {
		if m.CheckAuth(&sid, ctx) {
			return true
		}
	}

	b, _ := json.Marshal(&Msg{
		Code: -1,
		Msg:  "auth fail",
	})

	ctx.Response.Header.Set("Content-Type", "application/json")
	ctx.Response.SetBody(b)

	return false
}
