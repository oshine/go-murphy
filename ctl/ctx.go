package ctl

import (
	"github.com/goccy/go-json"
	"github.com/valyala/fasthttp"
	"log"
	"mime/multipart"
	"strconv"
)

type Ctx struct {
	_ctx   *fasthttp.RequestCtx
	_form  *multipart.Form
	_p_arg *fasthttp.Args
}

func (m *Ctx) File(p string) *multipart.FileHeader {
	if m._form == nil {
		return nil
	}
	var r = m._form.File[p]
	if len(r) > 0 {
		return r[0]
	}
	return nil
}

func (m *Ctx) WithCtx(ctx *fasthttp.RequestCtx) *Ctx {
	m._ctx = ctx
	m._p_arg = ctx.Request.PostArgs()
	m._form, _ = ctx.Request.MultipartForm()
	return m
}

func (m *Ctx) GetForm() *multipart.Form {
	return m._form
}

func (m *Ctx) Header(p string) string {
	return string(m._ctx.Request.Header.Peek(p))
}

func (m *Ctx) SetHeader(k, v string) {
	m._ctx.Response.Header.Set(k, v)
}

func (m *Ctx) SetStatusCode(c int) {
	m._ctx.Response.Header.SetStatusCode(c)
}

func (m *Ctx) Redirect(uri string) {
	m._ctx.Response.Header.SetStatusCode(302)
	m._ctx.Response.Header.Set("Location", uri)
}

type AnyParam struct {
	v string
}

func (a *AnyParam) ToInt64() int64 {
	f, err := strconv.ParseInt(a.v, 10, 64)
	if err != nil {
		return 0
	}
	return f
}

func (a *AnyParam) MustInt64() (int64, error) {
	return strconv.ParseInt(a.v, 10, 64)
}

func (a *AnyParam) ToFloat64() float64 {
	f, err := strconv.ParseFloat(a.v, 64)
	if err != nil {
		return 0
	}
	return f
}

func (a *AnyParam) MustFloat64() (float64, error) {
	return strconv.ParseFloat(a.v, 64)
}

// AnyUriParam get an arg from query uri
func (m *Ctx) AnyUriParam(p string) (ap *AnyParam) {
	ap = &AnyParam{}

	ap.v = string(m._ctx.QueryArgs().Peek(p))

	return
}

// UriParam get an arg from query uri
func (m *Ctx) UriParam(p string) string {
	return string(m._ctx.QueryArgs().Peek(p))
}

// AnyMultiParam get arg with content-type: multipart/form-data
func (m *Ctx) AnyMultiParam(p string) (ap *AnyParam) {
	ap = &AnyParam{}
	if m._form == nil {
		return
	}
	var r = m._form.Value[p]
	if len(r) > 0 {
		ap.v = r[0]
		return
	}
	return
}

// MultiParam get arg with content-type: multipart/form-data
func (m *Ctx) MultiParam(p string) string {
	if m._form == nil {
		return ""
	}
	var r = m._form.Value[p]
	if len(r) > 0 {
		return r[0]
	}
	return ""
}

// AnyPostParam get arg with content-type: application/x-www-form-urlencoded
func (m *Ctx) AnyPostParam(p string) (ap *AnyParam) {
	ap = &AnyParam{v: string(m._p_arg.Peek(p))}
	return
}

// PostParam get arg with content-type: application/x-www-form-urlencoded
func (m *Ctx) PostParam(p string) string {
	return string(m._p_arg.Peek(p))
}

func (m *Ctx) Success(msg string) error {
	p := &Msg{_ctr: m}
	p.Ok(msg)
	return p.Out()
}

func (m *Ctx) Error(err string) error {
	p := &Msg{_ctr: m}
	p.Err(err)
	return p.Out()
}

func (m *Ctx) Html(str string) error {
	m._ctx.SetContentType("text/html; charset=utf-8")
	m._ctx.SetBodyString(str)
	return nil
}

func (m *Ctx) Text(str string) error {
	m._ctx.SetBodyString(str)
	return nil
}

func (m *Ctx) RenderJson(data any) *Msg {
	return &Msg{Data: data, _ctr: m}
}

type Msg struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
	_ctr *Ctx
}

func (msg *Msg) Ok(str string) *Msg {
	msg.Msg = str
	msg.Code = 0
	return msg
}

func (msg *Msg) Err(str string) *Msg {
	msg.Msg = str
	msg.Code = -1
	return msg
}

func (msg *Msg) Out() error {
	b, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	ctx := msg._ctr._ctx

	{
		// TODO if you Know the meaning of this block, remove this block.
		if ctx == nil {
			log.Println("You must set ctx before output data.")
			log.Println(string(b))
			return nil
		}
	}
	ctx.Response.Header.Set("Content-Type", "application/json")
	_, err = ctx.Response.BodyWriter().Write(b)
	return err
}
