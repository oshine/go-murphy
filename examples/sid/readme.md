## sid 鉴权用例

### 1.运行方法：
```shell
cd examples/sid

go run main.go
```

### 2.访问首页
注意：windows下使用gitbash运行以下命令
```shell
curl  'http://127.0.0.1:18000/'
```

### 2.登录

```shell
curl -fSsl --location --request GET 'http://127.0.0.1:18000/api/member/login' \
--header 'User-Agent: murphy' \
--form 'username="admin"' \
--form 'password="admin"'
```
### 2.获取详情

```shell
curl  -fSsl --location --request GET 'http://127.0.0.1:18000/api/member/detail' \
--header 'sid: 100.d715067c055689e1dc8b2421d917670a709b28e11' \
--header 'User-Agent: murphy' \
--form 'name="admin"'
```