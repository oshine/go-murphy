package main

import (
	"gitee.com/oshine/murphy/ctl"
	"gitee.com/oshine/murphy/examples/sid/model"
)

type BaseController struct {
	ctl.SidController
	MemberId *int64
	Member   *model.Member
}

// 无需鉴权的路由/the router which is no need to auth
var skipRouter = [][]string{
	{"member", "index", "login"},
}

func (self *BaseController) Init() {
	//member := model.NewMember()
	//self.Member = member
	//self.MemberId = &member.GetDao().Id

	member := &model.Member{}
	self.Member = member
	self.MemberId = &member.Id

	self.SetSkipRule(&skipRouter)
	self.InitSid(member)
}
