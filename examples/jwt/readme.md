## sid 鉴权用例

### 1.运行方法：
```shell
cd examples/sid

go run main.go
```

### 2.访问首页
注意：windows下使用gitbash运行以下命令
```shell
curl  'http://127.0.0.1:18000/'
```

### 2.登录

```shell
curl -fSsl --location --request GET 'http://127.0.0.1:18000/api/member/login' \
--header 'User-Agent: murphy' \
--form 'username="admin"' \
--form 'password="admin"'
```
### 2.获取详情

```shell
curl  -fSsl --location --request GET 'http://127.0.0.1:18000/api/member/detail' \
--header 'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJRCI6MTAwLCJ1c2VybmFtZSI6IiIsImV4cCI6MTcyMjY3MTY4MywiaXNzIjoibXVycGh5In0.-FpQfNt3caxTh8KTqV2B_AJ099FT3BCqjPK7tQBa2e8' \
--header 'User-Agent: murphy' \
--form 'name="admin"'
```