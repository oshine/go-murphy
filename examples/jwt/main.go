package main

import (
	"gitee.com/oshine/murphy/core"
)

func main() {
	svr := core.Server{}
	svr.Http().
		Group("/api", func(group *core.SvrGroup) {

			group.Auth("Auth")
			group.Home("/member/index")

			group.Bind(
				&MemberController{},
			)

		}).RunTLS("127.0.0.1:18000", "", "")
}
