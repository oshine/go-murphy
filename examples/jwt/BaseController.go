package main

import (
	"gitee.com/oshine/murphy/ctl"
)

var signKey = []byte("89234yutr97835y789t390247yu893th3")

type BaseController struct {
	ctl.JwtController
	Id int64
	//member *Member
}

var skipRule = [][]string{
	{"member", "index", "login"},
}

func (c *BaseController) Init() {

	c.SetSkipRule(&skipRule)
	c.InitJwt(signKey)

	c.AuthOk = func(id *int64) bool {
		c.Id = *id
		return true
	}
}
