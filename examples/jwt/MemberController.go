package main

import (
	"gitee.com/oshine/murphy/core/text"
	"gitee.com/oshine/murphy/ctl"
	"github.com/valyala/fasthttp"
	"log"
	"strconv"
)

type MemberController struct {
	BaseController
}

// Index 首页
func (self *MemberController) Index(ctx *fasthttp.RequestCtx) error {
	self.WithCtx(ctx)

	method := string(ctx.Request.Header.Method())
	log.Println(method)

	if text.InArray(method, []string{"GET", "POST", "PUT", "DELETE"}) {

		return self.RenderJson(nil).Ok("Hello World").Out()
	}

	return self.RenderJson(nil).Ok("Not Support Method:" + method).Out()
}

// Login 登录
func (self *MemberController) Login(ctx *fasthttp.RequestCtx) error {
	self.WithCtx(ctx)

	username := self.MultiParam("username")
	password := self.MultiParam("password")
	log.Println("login:", username, password)

	//id:= Member.FindFirst(username,password)
	id := 100

	token, _ := ctl.GenerateToken(int64(id), 7200, signKey)

	return self.RenderJson(map[string]string{
		"token": token,
	}).Ok("login success").Out()
}

// Detail 详情
func (self *MemberController) Detail(ctx *fasthttp.RequestCtx) error {
	self.WithCtx(ctx)

	name := self.MultiParam("name")
	log.Println("detail:", name)

	return self.RenderJson(map[string]string{
		"id":   strconv.FormatInt(self.Id, 10),
		"name": name,
	}).Ok("").Out()
}
