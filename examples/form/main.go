package main

import (
	"gitee.com/oshine/murphy/core"
	"gitee.com/oshine/murphy/ctl"
	"github.com/valyala/fasthttp"
	"log"
)

type HomeController struct {
	ctl.EmptyController
}

func (self *HomeController) Index(ctx *fasthttp.RequestCtx) error {
	self.WithCtx(ctx)

	html := `<html><body>
	<div>用户名为:admin,密码为:20</div>
    <form method="POST" action="/api/home/submit">
        <label for="name">Name:</label>
        <input type="text" id="name" name="name">
        <input type="text" id="age" name="age">
        <input type="submit" value="Submit">
    </form>
    </body></html>`

	ctx.SetContentType("text/html; charset=utf-8")

	ctx.Write([]byte(html))

	return nil
}

func (self *HomeController) Submit(ctx *fasthttp.RequestCtx) error {
	self.WithCtx(ctx)

	n := ctx.PostArgs().Peek("name")
	log.Println(n)

	// 获取参数name
	name := self.PostParam("name")
	//name := self.MultiParam("name")  // if content-type == multipart/form-data , use MultiParam()

	// 获取参数age
	age := self.AnyPostParam("age").ToInt64()

	log.Println(name, age)

	if name == "admin" && age == 20 {
		return self.Success("密码正确")
	}

	return self.Error("密码错误")
}

func main() {
	svr := &core.Server{}
	svr.Http().
		Group("/api", func(group *core.SvrGroup) {

			group.Home("/home/index") // let "/" = api/"home/index"

			group.Bind(
				&HomeController{}, // home
				//&OtherController{},
				//&***Controller{},
			)

		}).RunTLS("127.0.0.1:8000", "", "")

}
