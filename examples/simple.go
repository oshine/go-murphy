package main

import (
	"gitee.com/oshine/murphy/core"
	"gitee.com/oshine/murphy/core/text"
	"gitee.com/oshine/murphy/ctl"
	"github.com/valyala/fasthttp"
	"log"
)

type HomeController struct {
	ctl.EmptyController
}

func (self *HomeController) Index(ctx *fasthttp.RequestCtx) error {
	self.WithCtx(ctx)

	method := string(ctx.Request.Header.Method())
	uri := string(ctx.Request.URI().Path())
	fun := text.SubEx(text.TEXT_SUB_NORMAL, uri, "/")

	log.Println(method, fun)

	if text.InArray(method, []string{"GET", "POST", "PUT"}) {

		//return self.Success"Hello World"()

		return self.RenderJson(nil).Ok("Hello World").Out()

	}

	return self.Error("Not Support Method:" + method)
}

func main() {
	svr := &core.Server{}
	svr.Http().
		Group("/api", func(group *core.SvrGroup) {

			group.Home("/home/index") // let "/" = api/"home/index"

			group.Bind(
				&HomeController{}, // home
				//&OtherController{},
				//&***Controller{},
			)

		}).RunTLS("127.0.0.1:8000", "", "")

}
