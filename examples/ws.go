package main

import (
	"gitee.com/oshine/murphy/core"
	"github.com/fasthttp-contrib/websocket"
)

func WsV1(c *websocket.Conn) error {
	err := c.WriteMessage(websocket.TextMessage, []byte("Hello World!"))
	if err != nil {
		return err
	}
	return nil
}

func main() {
	svr := core.Server{}
	svr.Ws().
		//MiddleWare(WsV1).
		Router("/ws-v1", WsV1).
		//Router("/ws-v2", ws.WsV2).
		RunTLS("127.0.0.1:8006", "", "")
}
